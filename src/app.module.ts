import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SplitService } from './split/split.service';
@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, SplitService],
})
export class AppModule { }
