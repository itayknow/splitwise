import { Controller, Post,Body } from '@nestjs/common';
import { PaymentRequest } from './entities/paymentRequest';
import { SplitService } from './split/split.service';

@Controller()
export class AppController {
  constructor(private readonly splitService: SplitService) {}

  @Post()
  newPayment(@Body() payment: PaymentRequest) {
    return this.splitService.payment(payment);
  }
}
