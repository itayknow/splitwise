import { Injectable } from '@nestjs/common';
import { Group } from '../entities/group';
import { Participent } from '../entities/participent';
import { PaymentRequest } from '../entities/paymentRequest';
import { User } from '../entities/user';

@Injectable()
export class SplitService {
    async payment(payment: PaymentRequest) {
        const participents = payment.payers.map(payer => {
            return {
                payer: { id: payer.payerId },
                amount: payer.amount
            }
        });

        console.log(participents);
        console.log("Splitted amount: " + payment.totalPayment / participents.length);
        const lendersAndBorrowers = this.getLendersAndBorrowers(participents, payment.totalPayment);
        const debts = this.splitLogic(lendersAndBorrowers.lenders, lendersAndBorrowers.borrowers);

        console.log(debts);
        return debts;
    }

    splitLogic(lenders: Map<string, number>, borrowers: Map<string, number>) {
        let debts: { payTo: string, payFrom: string, amount: number }[] = [];
        let newLenders = lenders;
        let newBorrowers = borrowers;
        Array.from(lenders.keys()).forEach((lenderId: string) => {
            Array.from(borrowers.keys()).forEach((borrowerId: string) => {
                // this.getPayingAmountDiff({ id: lenderId, lendedAmount: newLenders.get(lenderId) }, { id: borrowerId, borrowedAmount });
                if ((newLenders.get(lenderId) - newBorrowers.get(borrowerId)) >= 0) {
                    debts.push({
                        payFrom: borrowerId,
                        payTo: lenderId,
                        amount: newBorrowers.get(borrowerId)
                    });
                    newLenders.set(lenderId, newLenders.get(lenderId) - newBorrowers.get(borrowerId));
                    newBorrowers.set(borrowerId, 0);
                } else {
                    debts.push({
                        payFrom: borrowerId,
                        payTo: lenderId,
                        amount: newLenders.get(lenderId)
                    });
                    newBorrowers.set(borrowerId, newBorrowers.get(borrowerId) - newLenders.get(lenderId));
                    newLenders.set(lenderId, 0)
                }
            })
        });

        return debts;
    }

    // getPayingAmountDiff(lender: { id: string, lendedAmount: number }, borrower: { id: string, borrowedAmount: number }) {
    //     if ((lender.lendedAmount - borrower.borrowedAmount) >= 0) {
    //         return {
    //             payFrom: borrower.id,
    //             payTo: lender.id,
    //             amount: borrower.borrowedAmount
    //         }
    //     } else
    //         if ((borrower.borrowedAmount - lender.lendedAmount) >= borrower.borrowedAmount) {
    //             return {
    //                 payFrom: borrower.id,
    //                 payTo: lender.id,
    //                 amount: borrower.borrowedAmount - lender.lendedAmount
    //             };
    //         }
    // }


    getLendersAndBorrowers(participents: Participent[], totalPayment: number): { lenders: Map<string, number>, borrowers: Map<string, number> } {
        let lenders: Map<string, number> = new Map();
        let borrowers: Map<string, number> = new Map();
        const splittedPayment = Math.round(totalPayment / participents.length);
        participents.forEach(payer => {
            (payer.amount - splittedPayment > 0) ?
                lenders.set(payer.payer.id, payer.amount - splittedPayment) :
                borrowers.set(payer.payer.id, splittedPayment - payer.amount)
        })

        return { lenders, borrowers };
    }

    fetchUsers(ids: string[]): User[] {
        return []
    }

    fetchGroup(id: string[]): Group[] {
        return [];
    }

    fetchUserGroup(groupId: string, userId: string): Group {
        return null;
    }
}
