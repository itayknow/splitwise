import { User } from "./user";

export interface Participent {
    payer: User,
    amount: number
}