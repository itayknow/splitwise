import { User } from "./user";

export interface Group {
    id: string,
    users: User[],
}