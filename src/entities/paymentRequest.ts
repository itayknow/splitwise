export interface PaymentRequest {
    payers: { payerId: string, amount: number }[];
    participentsIds: string[];
    totalPayment: number;
    groupId: string;
    description: string;
};