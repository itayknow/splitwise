export interface UserInGroup {
    id: string;
    userId: string;
    groupId: string;
    balance: number;
    transfers: number;
    paiedBy: { userId: string, debt: number, date: string }[];
    owesTo: { userId: string, debt: number }[];
}